import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatToolbarModule, MatIconModule, MatDividerModule,
  MatListModule, MatButtonModule, MatDialogModule, MatCardModule
} from '@angular/material';
@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    MatToolbarModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatDividerModule
  ]
})
export class MaterialModule { }
