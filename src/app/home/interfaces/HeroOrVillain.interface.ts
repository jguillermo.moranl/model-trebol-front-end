export interface HeroOrVillain {
    name: string;
    skill: string[];
    city: string;
    category: string;
    avatar: string;
    mision: boolean;
    battlesWon: number;
}
