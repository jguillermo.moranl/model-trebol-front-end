import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { HeroOrVillain } from '../interfaces/HeroOrVillain.interface';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ViewProfileComponent } from '../view-profile/view-profile.component';

@Component({
  selector: 'app-list-heroes',
  templateUrl: './list-heroes.component.html',
  styleUrls: ['./list-heroes.component.scss']
})
export class ListHeroesComponent implements OnInit {

  @Input() heroes: Observable<HeroOrVillain[]>

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  OpenProfile = (hero: HeroOrVillain): void => {
    const config: MatDialogConfig = {

    }
    this.dialog.open(ViewProfileComponent, {
      height: '600px',
      width: '400px',
      data: hero
    });
  }


}
